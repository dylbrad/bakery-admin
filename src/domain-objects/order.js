import Customer from "domain-objects/customer";
import Delivery from "domain-objects/delivery";
import Trolley from "domain-objects/trolley";
import TrolleyItem from "domain-objects/trolley-item";
import Payment from "domain-objects/payment";

import { formatDateTime } from "utils/date";

export default class Order {
  constructor({
    id,
    delivery = new Delivery(),
    trolley = new Trolley(),
    payment = new Payment(),
    customer = new Customer(),
    status_id,
    message = "",
    inserted_at,
    updated_at,
    is_delivery = true,
  } = {}) {
    this.id = id;
    this.delivery = delivery;
    this.trolley = trolley;
    this.payment = payment;
    this.customer = customer;
    this.status_id = status_id;
    this.message = message;
    this.inserted_at = inserted_at;
    this.updated_at = updated_at;
    this.is_delivery = is_delivery;
  }

  formatField(field) {
    switch (field) {
      case "updated_at":
      case "inserted_at": {
        return this[field] && formatDateTime(this[field]);
      }
      default: {
        return this[field];
      }
    }
  }

  isSame(order) {
    return (
      this.id === order.id &&
      this.delivery.isSame(order.delivery) &&
      this.trolley.isSame(order.trolley) &&
      this.payment.isSame(order.payment) &&
      this.customer.isSame(order.customer) &&
      this.status_id === order.status_id &&
      this.message === order.message &&
      this.inserted_at === order.inserted_at &&
      this.updated_at === order.updated_at &&
      this.is_delivery === order.is_delivery
    );
  }

  update(field, value) {
    return new Order({ ...this, [field]: value });
  }

  updateIsDelivery(value) {
    return new Order({
      ...this,
      is_delivery: value,
      trolley: new Trolley({
        ...this.trolley,
        delivery_price: value === true ? 3 : 0,
      }),
    });
  }

  renderJSON() {
    return {
      id: this.id,
      status_id: this.status_id,
      message: this.message,
      delivery: this.delivery.renderJSON(),
      trolley: this.trolley.renderJSON(),
      payment: this.payment.renderJSON(),
      customer: this.customer.renderJSON(),
      is_delivery: this.is_delivery,
    };
  }
}

export function createOrder(params) {
  return new Order({
    ...params,
    delivery: new Delivery(params.delivery),
    trolley: new Trolley({
      ...params.trolley,
      items: params.trolley.items.map((item) => new TrolleyItem(item)),
    }),
    payment: new Payment(params.payment),
    customer: new Customer(params.customer),
  });
}
