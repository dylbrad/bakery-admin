import { formatDateTime } from "utils/date";

export default class Message {
  constructor({ id, message = "", inserted_at, email, name } = {}) {
    this.id = id;
    this.message = message;
    this.inserted_at = inserted_at;
    this.name = name;
    this.email = email;
  }

  renderInsertedAt() {
    return formatDateTime(this.inserted_at);
  }
}
