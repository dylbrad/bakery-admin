export default class Setting {
  constructor({ id, name, value, type } = {}) {
    this.id = id;
    this.name = name;
    this.value = value;
    this.type = type;
  }

  update(value) {
    return new Setting({ ...this, value });
  }

  renderJSON() {
    return {
      id: this.id,
      name: this.name,
      value: this.value,
      type: this.type,
    }
  }
}
