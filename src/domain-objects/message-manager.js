import Message from "domain-objects/message";

export default class MessageManager {
  constructor(items = []) {
    this.items = items;
  }
}

export function createMessageManager(items) {
  return new MessageManager(items.map((message) => new Message(message)));
}
