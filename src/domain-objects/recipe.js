export default class Product {
  constructor({ id, recipe = "", product_id } = {}) {
    this.id = id;
    this.recipe = recipe;
    this.product_id = product_id;
  }

  update(field, value) {
    return new Product({ ...this, [field]: value });
  }

  renderJSON() {
    return {
      recipe: {
        id: this.id,
        recipe: this.recipe,
      },
    };
  }
}
