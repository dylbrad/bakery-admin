import SpecialOffer from "domain-objects/special-offer";

export default class SpecialOfferManager {
  constructor(items = []) {
    this.items = items;
  }

  getSpecialOffer(id) {
    return this.items.find((item) => item.id === id) || new SpecialOffer();
  }
}

export function createSpecialOfferManager(items) {
  return new SpecialOfferManager(
    items.map((specialOffer) => new SpecialOffer(specialOffer))
  );
}
