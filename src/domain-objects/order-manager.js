import { MONTHS, isDateSame, isMonthSame } from "utils/date";

export default class OrderManager {
  constructor(items = []) {
    this.items = items;
  }

  calculateTotalSold() {
    return this.items.reduce(
      (total, order) => total + order.trolley.calculateTotal(),
      0
    );
  }

  filterByID(ids) {
    return new OrderManager(
      this.items.filter((order) => ids.indexOf(order.id) !== -1)
    );
  }

  filterStatus(status_id) {
    return new OrderManager(
      this.items.filter((order) => order.status_id === status_id)
    );
  }

  filterDate(filterDate) {
    let date = null;
    if (filterDate === "today") {
      date = new Date();
    } else if (filterDate === "tomorrow") {
      const tomorrow = new Date();
      tomorrow.setDate(new Date().getDate() + 1);
      date = tomorrow;
    } else {
      return this;
    }
    return new OrderManager(
      this.items.filter((order) =>
        isDateSame(order.delivery.delivery_date, date)
      )
    );
  }

  filterMonth(month) {
    const monthForString = `${MONTHS.indexOf(month) + 1}`.padStart(2, "0");
    return new OrderManager(
      this.items.filter((order) => {
        const deliveryDate = new Date(order.delivery.delivery_date);
        return isMonthSame(
          order.delivery.delivery_date,
          new Date(`${deliveryDate.getFullYear()}-${monthForString}-01`)
        );
      })
    );
  }

  groupByProduct(specialOfferManager) {
    return this.items
      .map((order) =>
        order.trolley.items.map((trolleyItem) => {
          if (trolleyItem.product_id) {
            return {
              product_id: trolleyItem.product_id,
              quantity: trolleyItem.quantity,
              unit_price: trolleyItem.unit_price,
            };
          }
          return specialOfferManager
            .getSpecialOffer(trolleyItem.special_offer_id)
            .products.map((productID) => ({
              product_id: productID,
              quantity: 1 * trolleyItem.quantity,
              unit_price: 0,
            }));
        })
      )
      .flat(2)
      .reduce((accumulator, item) => {
        function addProduct(productID, quantity = 1) {
          const accItem = accumulator.find(
            (accItem) => accItem.product_id === item.product_id
          );
          const index = accumulator.indexOf(accItem);
          if (index !== -1) {
            return [
              ...accumulator.slice(0, index),
              {
                product_id: item.product_id,
                quantity: accItem.quantity + item.quantity,
                unit_price: item.unit_price,
              },
              ...accumulator.slice(index + 1),
            ];
          } else {
            return [
              ...accumulator,
              {
                product_id: item.product_id,
                quantity: item.quantity,
                unit_price: item.unit_price,
              },
            ];
          }
        }
        return addProduct(item.product_id, item.quantity);
      }, []);
  }

  groupByDate() {
    return this.items.reduce((accumulator, order) => {
      const orderInAcc = accumulator.find(
        ({ delivery_date }) =>
          delivery_date === order.delivery.formatField("delivery_date")
      );
      if (orderInAcc) {
        return [
          ...accumulator.slice(0, accumulator.indexOf(orderInAcc)),
          {
            ...orderInAcc,
            orderManager: new OrderManager([
              ...orderInAcc.orderManager.items,
              order,
            ]),
          },
          ...accumulator.slice(accumulator.indexOf(orderInAcc) + 1),
        ];
      }
      return [
        ...accumulator,
        {
          orderManager: new OrderManager([order]),
          delivery_date: order.delivery.formatField("delivery_date"),
        },
      ];
    }, []);
  }
}
