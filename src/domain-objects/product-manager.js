import Product from "domain-objects/product";
import AllergenManager from "domain-objects/allergen-manager";

export default class ProductManager {
  constructor(items = []) {
    this.items = items;
  }

  getProduct(productID) {
    return this.items.find((item) => item.id === productID) || new Product();
  }

  groupProducts() {
    return this.items.reduce((accumulator, thisProduct) => {
      // Is the product from this.items present in the accumulator?
      const item = accumulator.find(
        ({ product, quantity }) => thisProduct.id === product.id
      );
      // If it is not present, we add it to the accumulator with a quantity of 1
      if (!item) {
        return [...accumulator, { quantity: 1, product: thisProduct }];
      }
      // If it is present, we need to edit the accumulator item to add 1 as the quantity
      return [
        ...accumulator.slice(0, accumulator.indexOf(item)),
        { quantity: item.quantity + 1, product: thisProduct },
        ...accumulator.slice(accumulator.indexOf(item) + 1),
      ];
    }, []);
  }

  getAllergens(allergenManager) {
    return new AllergenManager([
      ...new Set(
        this.items
          .map((product) => product.getAllergens(allergenManager).items)
          .flat()
      ),
    ]);
  }

  getMayContainAllergens(allergenManager) {
    return new AllergenManager([
      ...new Set(
        this.items
          .map(
            (product) => product.getMayContainAllergens(allergenManager).items
          )
          .flat()
      ),
    ]);
  }
}
