import { formatDate } from "utils/date";

export default class Delivery {
  constructor({ delivery_date = null, time_slot, message = "" } = {}) {
    this.delivery_date = delivery_date;
    this.time_slot = time_slot;
    this.message = message;
  }

  isValid() {
    return this.delivery_date !== null && this.time_slot !== null;
  }

  isSame(delivery) {
    return (
      this.delivery_date === delivery.delivery_date &&
      this.time_slot === delivery.time_slot &&
      this.message === delivery.message
    );
  }

  formatField(field) {
    switch (field) {
      case "delivery_date": {
        return this.delivery_date && formatDate(this.delivery_date);
      }
      default: {
        return this[field];
      }
    }
  }

  update(field, value) {
    return new Delivery({ ...this, [field]: value });
  }

  renderJSON() {
    return {
      delivery_date: this.delivery_date,
      time_slot: this.time_slot,
      message: this.message,
    };
  }
}
