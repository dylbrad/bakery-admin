export default class Session {
  constructor({ email = "", password = "" } = {}) {
    this.email = email;
    this.password = password;
  }

  update(field, value) {
    return new Session({ ...this, [field]: value });
  }

  renderJSON() {
    return { email: this.email, password: this.password };
  }
}
