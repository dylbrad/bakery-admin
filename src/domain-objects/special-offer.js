import ProductManager from "domain-objects/product-manager";

export default class SpecialOffer {
  constructor({
    id,
    disabled = true,
    name = "",
    order,
    price,
    image = "",
    products = [],
  } = {}) {
    this.id = id;
    this.disabled = disabled;
    this.name = name;
    this.order = order;
    this.price = price;
    this.image = image;
    this.products = products;
  }

  getProducts(productManager = new ProductManager()) {
    if (!productManager.items.length) return new ProductManager();
    return new ProductManager(
      this.products.map((productID) =>
        productManager.items.find((product) => product.id === productID)
      )
    );
  }

  update(field, value) {
    return new SpecialOffer({ ...this, [field]: value });
  }

  addProduct(productID) {
    return new SpecialOffer({
      ...this,
      products: [...this.products, productID],
    });
  }

  removeProduct(productID) {
    if (this.products.indexOf(productID) === -1) {
      return this;
    }
    return new SpecialOffer({
      ...this,
      products: [
        ...this.products.slice(0, this.products.indexOf(productID)),
        ...this.products.slice(this.products.indexOf(productID) + 1),
      ],
    });
  }

  renderJSON() {
    return {
      id: this.id,
      disabled: this.disabled,
      name: this.name,
      order: this.order,
      price: this.price,
      image: this.image,
      products: this.products,
    };
  }
}
