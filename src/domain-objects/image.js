export default class Image {
  constructor({ id, url = "" } = {}) {
    this.id = id;
    this.url = url;
  }
}
