export function beginningOfWeek(date) {
  const monday = date.clone().subtract(date.day() - 1, "day");
  monday.set("hour", 0);
  monday.set("minute", 0);
  monday.set("second", 0);
  return monday;
}

export function addDays(date, days) {
  return date.clone().add(days, "day");
}

export function removeDays(date, days) {
  return date.clone().subtract(days, "day");
}

export function addHours(date, hours) {
  return date.clone().add(hours, "hour");
}
