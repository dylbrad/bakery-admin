import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Container, Label, Menu } from "semantic-ui-react";
import { Link, useLocation, useHistory } from "react-router-dom";

import Header from "layouts/header";
import { logout, isUserLoggedIn } from "redux/actions/session";
import { getMessages, clearMessages } from "redux/actions/message-manager";
import { getSettings, clearSettings } from "redux/actions/setting-manager";
import CustomerAppButton from "components/customer-app-button";

const AppLayout = ({
  children,
  logout,
  messageManager,
  getMessages,
  clearMessages,
  getSettings,
  clearSettings,
  settingManager,
}) => {
  const { pathname } = useLocation();
  const history = useHistory();

  useEffect(() => {
    getSettings();
    if (isUserLoggedIn()) {
      getMessages();
    }
    return () => {
      clearMessages();
      clearSettings();
    };
  }, [getMessages, clearMessages, getSettings, clearSettings]);

  useEffect(() => {
    const favicon = document.getElementById("favicon");
    const adminFavicon = settingManager.getByName("admin_favicon").value;
    const bakeryName = settingManager.getByName("bakery_name").value;
    if (adminFavicon) {
      favicon.href = adminFavicon;
    }
    if (bakeryName) {
      document.title = `${bakeryName} admin`;
    }
  }, [settingManager]);

  return (
    <Container>
      <Header />
      <CustomerAppButton />
      {isUserLoggedIn() && (
        <Segment>
          <Menu stackable>
            <Menu.Item
              active={pathname.startsWith("/orders")}
              as={Link}
              link
              to="/orders"
            >
              Orders
            </Menu.Item>
            <Menu.Item
              active={pathname.startsWith("/products")}
              as={Link}
              link
              to="/products"
            >
              Products
            </Menu.Item>
            <Menu.Item
              active={pathname.startsWith("/special-offers")}
              as={Link}
              link
              to="/special-offers"
            >
              Special Offers
            </Menu.Item>
            <Menu.Item
              active={pathname.startsWith("/recipes")}
              as={Link}
              link
              to="/recipes"
            >
              Recipes
            </Menu.Item>
            <Menu.Item
              active={pathname.startsWith("/images")}
              as={Link}
              link
              to="/images"
            >
              Images
            </Menu.Item>
            <Menu.Item
              active={pathname.startsWith("/messages")}
              as={Link}
              link
              to="/messages"
            >
              Messages
              <Label>{messageManager.items.length}</Label>
            </Menu.Item>
            <Menu.Item
              active={pathname.startsWith("/booking-slots")}
              as={Link}
              link
              to="/booking-slots"
            >
              Booking Slots
            </Menu.Item>
            <Menu.Item
              active={pathname.startsWith("/reports")}
              as={Link}
              link
              to="/reports"
            >
              Reports
            </Menu.Item>
            <Menu.Item
              active={pathname.startsWith("/settings")}
              as={Link}
              link
              to="/settings"
            >
              Settings
            </Menu.Item>
            <Menu.Item
              position="right"
              onClick={async () => {
                await logout();
                history.push("/login");
              }}
            >
              Logout
            </Menu.Item>
          </Menu>
        </Segment>
      )}
      {children}
    </Container>
  );
};

const mapStateToProps = ({ messageManager, settingManager }) => ({
  messageManager,
  settingManager,
});

const mapDispatchToProps = {
  logout,
  getMessages,
  clearMessages,
  getSettings,
  clearSettings,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppLayout);
