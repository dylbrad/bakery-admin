import React from "react";
import { connect } from "react-redux";
import { Header as SHeader, Image } from "semantic-ui-react";
import { Link } from "react-router-dom";

const Header = ({ settingManager }) => {
  return (
    <Link to="/">
      <Image
        src={settingManager.getByName("app_logo").value}
        size="small"
        style={{
          display: "block",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      />
      <SHeader as="h2" icon textAlign="center">
        <SHeader.Content>
          {settingManager.getByName("bakery_name").value}
        </SHeader.Content>
        <SHeader sub>Admin website</SHeader>
      </SHeader>
    </Link>
  );
};

const mapStateToProps = ({ settingManager }) => ({ settingManager });

export default connect(mapStateToProps)(Header);
