import React from "react";

import { Placeholder, Grid } from "semantic-ui-react";

export default ({ columns = 1, rows = 15 }) => (
  <Grid columns={2}>
    {Array.from(new Array(columns), (item, index) => (
      <Grid.Column key={index}>
        <Placeholder>
          <Placeholder.Header>
            <Placeholder.Line />
            <Placeholder.Line />
          </Placeholder.Header>
          <Placeholder.Paragraph>
            {Array.from(new Array(rows), (item, index) => (
              <Placeholder.Line key={index} />
            ))}
          </Placeholder.Paragraph>
        </Placeholder>
      </Grid.Column>
    ))}
  </Grid>
);
