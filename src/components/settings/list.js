import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Button, Form, Message, Segment } from "semantic-ui-react";

import { getSettings, updateSettings } from "redux/actions/setting-manager";

const SettingList = ({ settingManager, getSettings, updateSettings }) => {
  const [stateSettingManager, setStateSettingManager] =
    useState(settingManager);
  const [message, setMessage] = useState(null);

  useEffect(() => {
    setStateSettingManager(settingManager);
  }, [setStateSettingManager, settingManager]);
  return (
    <Segment>
      {message && (
        <Message
          onDismiss={(event) => setMessage(null)}
          info
          content={message}
        />
      )}
      <Form
        onSubmit={async (event) => {
          event.preventDefault();
          await updateSettings(stateSettingManager);
          setMessage("Settings successfully updated");
        }}
      >
        <Form.Field>
          <label htmlFor="frontend_url">Frontend URL</label>
          <input
            type="text"
            name="frontend_url"
            onChange={(event) =>
              setStateSettingManager(
                settingManager.updateSetting(
                  event.target.name,
                  event.target.value
                )
              )
            }
            value={stateSettingManager.getByName("frontend_url").value}
          />
        </Form.Field>
        <Form.Field>
          <label htmlFor="instagram_url">Instagram URL</label>
          <input
            type="text"
            name="instagram_url"
            onChange={(event) =>
              setStateSettingManager(
                settingManager.updateSetting(
                  event.target.name,
                  event.target.value
                )
              )
            }
            value={stateSettingManager.getByName("instagram_url").value}
          />
        </Form.Field>
        <Form.Field>
          <label htmlFor="delivery_price">Delivery Price</label>
          <input
            type="text"
            name="delivery_price"
            onChange={(event) =>
              setStateSettingManager(
                settingManager.updateSetting(
                  event.target.name,
                  event.target.value
                )
              )
            }
            value={stateSettingManager.getByName("delivery_price").value}
          />
        </Form.Field>
        <Form.Field>
          <label htmlFor="baker_email">Baker's email</label>
          <input
            type="text"
            name="baker_email"
            onChange={(event) =>
              setStateSettingManager(
                settingManager.updateSetting(
                  event.target.name,
                  event.target.value
                )
              )
            }
            value={stateSettingManager.getByName("baker_email").value}
          />
        </Form.Field>
        <Form.Field>
          <label htmlFor="delivery_minimum_order">Delivery Minimum Order</label>
          <input
            type="text"
            name="delivery_minimum_order"
            onChange={(event) =>
              setStateSettingManager(
                settingManager.updateSetting(
                  event.target.name,
                  event.target.value
                )
              )
            }
            value={
              stateSettingManager.getByName("delivery_minimum_order").value
            }
          />
        </Form.Field>
        <Form.Field>
          <label htmlFor="collection_minimum_order">
            Collection Minimum Order
          </label>
          <input
            type="text"
            name="collection_minimum_order"
            onChange={(event) =>
              setStateSettingManager(
                settingManager.updateSetting(
                  event.target.name,
                  event.target.value
                )
              )
            }
            value={
              stateSettingManager.getByName("collection_minimum_order").value
            }
          />
        </Form.Field>
        <Form.Field>
          <label htmlFor="app_logo">Application Logo</label>
          <input
            type="text"
            name="app_logo"
            onChange={(event) =>
              setStateSettingManager(
                settingManager.updateSetting(
                  event.target.name,
                  event.target.value
                )
              )
            }
            value={stateSettingManager.getByName("app_logo").value}
          />
        </Form.Field>
        <Form.Field>
          <label htmlFor="frontend_favicon">Frontend Favicon</label>
          <input
            type="text"
            name="frontend_favicon"
            onChange={(event) =>
              setStateSettingManager(
                settingManager.updateSetting(
                  event.target.name,
                  event.target.value
                )
              )
            }
            value={stateSettingManager.getByName("frontend_favicon").value}
          />
        </Form.Field>
        <Form.Field>
          <label htmlFor="admin_favicon">Admin Favicon</label>
          <input
            type="text"
            name="admin_favicon"
            onChange={(event) =>
              setStateSettingManager(
                settingManager.updateSetting(
                  event.target.name,
                  event.target.value
                )
              )
            }
            value={stateSettingManager.getByName("admin_favicon").value}
          />
        </Form.Field>
        <Form.Field>
          <label htmlFor="bakery_name">Name of the bakery/shop</label>
          <input
            type="text"
            name="bakery_name"
            onChange={(event) =>
              setStateSettingManager(
                settingManager.updateSetting(
                  event.target.name,
                  event.target.value
                )
              )
            }
            value={stateSettingManager.getByName("bakery_name").value}
          />
        </Form.Field>
        <Button type="submit">Save settings</Button>
      </Form>
    </Segment>
  );
};

const mapStateToProps = ({ settingManager }) => ({
  settingManager,
});

const mapDispatchToProps = {
  getSettings,
  updateSettings,
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingList);
