import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

import SettingManager from "domain-objects/setting-manager";
import { updateSettings } from "redux/actions/setting-manager";
import AboutForm from "components/about/form";

const EditAbout = ({ settingManager, updateSettings }) => {
  const [stateSettingManager, setStateSettingManager] = useState(
    new SettingManager()
  );
  const history = useHistory();

  useEffect(() => {
    setStateSettingManager(settingManager);
  }, [setStateSettingManager, settingManager]);
  return (
    <AboutForm
      onSubmit={async (event) => {
        event.preventDefault();
        await updateSettings(stateSettingManager);
        history.push("/settings/about");
      }}
      settingManager={stateSettingManager}
      setSettingManager={setStateSettingManager}
      submitButtonText="Update About"
    />
  );
};

const mapStateToProps = ({ settingManager }) => ({ settingManager });

const mapDispatchToProps = { updateSettings };

export default connect(mapStateToProps, mapDispatchToProps)(EditAbout);
