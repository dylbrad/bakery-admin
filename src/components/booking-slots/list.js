import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Header, Label, Table } from "semantic-ui-react";
import moment from "moment";
import { Link, useParams } from "react-router-dom";

import { range } from "utils/util";
import { formatDate, formatDateToBackend } from "utils/date";
import { addDays, beginningOfWeek, removeDays } from "utils/moment";
import TimeSlot from "components/booking-slots/time-slot";
import { getBookingSlots } from "redux/actions/booking-slot-manager";

const BookingSlotList = ({ bookingSlotManager, getBookingSlots }) => {
  const { year, month, day } = useParams();
  const queryDate = moment.utc(`${year}-${month}-${day}`);
  const queryDateString = formatDateToBackend(queryDate);
  const days = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];

  useEffect(() => {
    getBookingSlots(queryDateString);
  }, [getBookingSlots, queryDateString]);

  const monday = beginningOfWeek(queryDate);
  monday.set("hour", 0);
  monday.set("minute", 0);
  const sunday = addDays(monday, 6);

  const previousMonday = removeDays(monday, 7);
  const nextMonday = addDays(monday, 7);
  return (
    <>
      From {formatDate(monday)} to {formatDate(sunday)}
      <br />
      <Header as="h3">
        Week {queryDate.week()}{" "}
        {moment().week() === queryDate.week() && (
          <Label color="green">Current</Label>
        )}
      </Header>
      <Link to={`/booking-slots/${formatDateToBackend(previousMonday)}`}>
        Previous week
      </Link>
      <Link to={`/booking-slots/${formatDateToBackend(nextMonday)}`}>
        Next week
      </Link>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell></Table.HeaderCell>
            {range(7).map((day) => (
              <Table.HeaderCell key={day}>
                {days[day]} {formatDate(addDays(monday, day))}
              </Table.HeaderCell>
            ))}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {range(5, 7).map((timeSlot) => {
            const timeSlotMoment = monday.clone().add(timeSlot, "hour");
            return (
              <Table.Row key={timeSlot}>
                <Table.Cell>
                  {timeSlotMoment.hour()}-{timeSlotMoment.hour() + 1}
                </Table.Cell>
                {range(7).map((day) => {
                  const date = timeSlotMoment.clone().add(day, "day");
                  return <TimeSlot date={date} timeSlot={timeSlot} key={day} />;
                })}
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    </>
  );
};

const mapStateToProps = ({ bookingSlotManager }) => ({
  bookingSlotManager,
});

const mapDispatchToProps = {
  getBookingSlots,
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingSlotList);
