import React from "react";
import { connect } from "react-redux";
import { Button, Icon, Menu, Table } from "semantic-ui-react";

import BookingSlot from "domain-objects/booking-slot";
import { addBookingSlots, resetBookingSlots } from "redux/actions/booking-slot";

const TimeSlot = ({
  addBookingSlots,
  resetBookingSlots,
  date,
  timeSlot,
  bookingSlotManager,
}) => {
  const bookedSlots = bookingSlotManager.getBookedSlots();
  const deliveryBookingSlotManager =
    bookingSlotManager.filterByIsDelivery(true);
  const collectionBookingSlotManager =
    bookingSlotManager.filterByIsDelivery(false);
  return (
    <Table.Cell key={`${date}-${timeSlot}`}>
      <Menu compact>
        <Menu.Item>
          <Icon
            name="bicycle"
            color={deliveryBookingSlotManager.length() > 0 ? "black" : "grey"}
          />{" "}
          <span
            style={{
              color: deliveryBookingSlotManager.length() > 0 ? "black" : "grey",
            }}
          >
            {deliveryBookingSlotManager.length()}
          </span>
        </Menu.Item>
        <Menu.Item>
          <Icon
            name="shopping bag"
            color={collectionBookingSlotManager.length() > 0 ? "black" : "grey"}
          />{" "}
          <span
            style={{
              color:
                collectionBookingSlotManager.length() > 0 ? "black" : "grey",
            }}
          >
            {collectionBookingSlotManager.length()}
          </span>
        </Menu.Item>
      </Menu>
      {bookedSlots.length() > 0 && `booked: ${bookedSlots.length()}`}
      <br />
      <Button.Group size="small">
        <Button compact size="mini" icon="bicycle" disabled color="black" />
        <Button
          compact
          size="mini"
          onClick={() =>
            addBookingSlots(
              new BookingSlot({ date, time_slot: timeSlot, is_delivery: true }),
              1
            )
          }
        >
          +1
        </Button>
        <Button
          compact
          size="mini"
          onClick={() => {
            addBookingSlots(
              new BookingSlot({ date, time_slot: timeSlot, is_delivery: true }),
              3
            );
          }}
        >
          +3
        </Button>
        <Button
          compact
          size="mini"
          onClick={() => resetBookingSlots(deliveryBookingSlotManager)}
        >
          Reset
        </Button>
      </Button.Group>
      <Button.Group size="small">
        <Button
          compact
          size="mini"
          icon="shopping bag"
          disabled
          color="black"
        />
        <Button
          compact
          size="mini"
          onClick={() =>
            addBookingSlots(
              new BookingSlot({
                date,
                time_slot: timeSlot,
                is_delivery: false,
              }),
              1
            )
          }
        >
          +1
        </Button>
        <Button
          compact
          size="mini"
          onClick={() => {
            addBookingSlots(
              new BookingSlot({
                date,
                time_slot: timeSlot,
                is_delivery: false,
              }),
              3
            );
          }}
        >
          +3
        </Button>
        <Button
          compact
          size="mini"
          onClick={() => resetBookingSlots(collectionBookingSlotManager)}
        >
          Reset
        </Button>
      </Button.Group>
    </Table.Cell>
  );
};

const mapStateToProps = ({ bookingSlotManager }, ownProps) => ({
  bookingSlotManager: bookingSlotManager.getSlots(
    ownProps.date,
    ownProps.timeSlot
  ),
});

const mapDispatchToProps = {
  addBookingSlots,
  resetBookingSlots,
};

export default connect(mapStateToProps, mapDispatchToProps)(TimeSlot);
