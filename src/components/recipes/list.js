import React from "react";
import { connect } from "react-redux";
import { Button, Table } from "semantic-ui-react";
import { Link } from "react-router-dom";

const RecipeList = ({ productManager, recipeManager }) => {
  return (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell></Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {recipeManager.items.map((recipe) => (
          <Table.Row key={recipe.id}>
            <Table.Cell>
              {productManager.getProduct(recipe.product_id).name}
            </Table.Cell>
            <Table.Cell>
              <Link to={`/recipes/${recipe.id}/edit`}>
                <Button>Edit</Button>
              </Link>
            </Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
};

const mapStateToProps = ({ recipeManager, productManager }) => ({
  recipeManager,
  productManager,
});

export default connect(mapStateToProps)(RecipeList);
