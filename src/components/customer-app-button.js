import React from "react";
import { connect } from "react-redux";
import { Button } from "semantic-ui-react";

const CustomerAppButton = ({ frontendURL }) =>
  frontendURL ? (
    <div style={{ overflow: "auto", paddingBottom: "1em" }}>
      <Button
        style={{ float: "right" }}
        as="a"
        href={frontendURL}
        target="_blank"
      >
        View customer app
      </Button>
    </div>
  ) : (
    ""
  );

const mapStateToProps = ({ settingManager }) => ({
  frontendURL: settingManager.getByName("frontend_url").value,
});

export default connect(mapStateToProps)(CustomerAppButton);
