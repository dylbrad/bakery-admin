import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Button, Checkbox, Image, Table } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { getProducts, clearProducts } from "redux/actions/product-manager";
import { updateProduct } from "redux/actions/product";

const ProductList = ({
  getProducts,
  productManager,
  clearProducts,
  updateProduct,
}) => {
  useEffect(() => {
    getProducts();
    return () => {
      clearProducts();
    };
  }, [getProducts, clearProducts]);

  return (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Key</Table.HeaderCell>
          <Table.HeaderCell>Description</Table.HeaderCell>
          <Table.HeaderCell>Image</Table.HeaderCell>
          <Table.HeaderCell>Price</Table.HeaderCell>
          <Table.HeaderCell>Order</Table.HeaderCell>
          <Table.HeaderCell>Enabled?</Table.HeaderCell>
          <Table.HeaderCell></Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {productManager.items.map((product) => (
          <Table.Row key={product.key}>
            <Table.Cell>{product.name}</Table.Cell>
            <Table.Cell>{product.key}</Table.Cell>
            <Table.Cell>{product.description}</Table.Cell>
            <Table.Cell>
              <Image size="small" src={product.image} />
            </Table.Cell>
            <Table.Cell>{product.price}</Table.Cell>
            <Table.Cell>{product.order}</Table.Cell>
            <Table.Cell>
              <Checkbox
                toggle
                onChange={async (event, data) => {
                  await updateProduct(
                    product.update("disabled", !data.checked)
                  );
                  getProducts();
                }}
                checked={!product.disabled}
              />
            </Table.Cell>
            <Table.Cell>
              <Link to={`/products/${product.id}/edit`}>
                <Button size="tiny" fluid>
                  Edit
                </Button>
              </Link>
            </Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
};

const mapStateToProps = ({ productManager }) => ({ productManager });
const mapDispatchToProps = { getProducts, clearProducts, updateProduct };

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
