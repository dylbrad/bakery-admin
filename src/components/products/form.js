import React from "react";
import { connect } from "react-redux";
import { Button, Form, Dropdown } from "semantic-ui-react";

const ProductForm = ({
  product,
  setProduct,
  onSubmit,
  allergenManager,
  submitButtonText = "Create Product",
}) => (
  <Form onSubmit={onSubmit}>
    <Form.Field>
      <label htmlFor="name">Name</label>
      <input
        placeholder="Name"
        name="name"
        id="name"
        value={product.name}
        onChange={(event) =>
          setProduct(product.update(event.target.name, event.target.value))
        }
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="key">Key</label>
      <input
        placeholder="Key"
        name="key"
        id="key"
        value={product.key}
        onChange={(event) =>
          setProduct(product.update(event.target.name, event.target.value))
        }
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="description">Description</label>
      <input
        placeholder="Description"
        name="description"
        id="description"
        value={product.description}
        onChange={(event) =>
          setProduct(product.update(event.target.name, event.target.value))
        }
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="image">Image</label>
      <input
        placeholder="Image"
        name="image"
        id="image"
        value={product.image}
        onChange={(event) =>
          setProduct(product.update(event.target.name, event.target.value))
        }
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="price">Price</label>
      <input
        placeholder="Price"
        name="price"
        id="price"
        value={product.price}
        onChange={(event) =>
          setProduct(product.update(event.target.name, event.target.value))
        }
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="order">Order</label>
      <input
        placeholder="Order"
        name="order"
        id="order"
        value={product.order || ""}
        onChange={(event) =>
          setProduct(product.update(event.target.name, event.target.value))
        }
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="allergens">Allergens</label>
      <Dropdown
        name="allergens"
        placeholder="Allergens"
        multiple
        selection
        options={allergenManager.items.map((allergen) => ({
          key: allergen.name,
          text: allergen.name,
          value: allergen.id,
        }))}
        onChange={(event, data) =>
          setProduct(product.update(data.name, data.value))
        }
        value={product.allergens}
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="may_contain_allergens">May Contain Allergens</label>
      <Dropdown
        name="may_contain_allergens"
        placeholder="May Contain Allergens"
        multiple
        selection
        options={allergenManager.items.map((allergen) => ({
          key: allergen.name,
          text: allergen.name,
          value: allergen.id,
        }))}
        onChange={(event, data) =>
          setProduct(product.update(data.name, data.value))
        }
        value={product.may_contain_allergens}
      />
    </Form.Field>
    <Button type="submit">{submitButtonText}</Button>
  </Form>
);

const mapStateToProps = ({ allergenManager }) => ({ allergenManager });

export default connect(mapStateToProps)(ProductForm);
