import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import { useHistory } from "react-router-dom";

import Product from "domain-objects/product";
import { updateProduct, getProduct, clearProduct } from "redux/actions/product";
import ProductForm from "components/products/form";

const UpdateProduct = ({
  updateProduct,
  getProduct,
  allergenManager,
  product,
}) => {
  const [stateProduct, setProduct] = useState(new Product());
  const history = useHistory();
  useEffect(() => {
    setProduct(product);
  }, [setProduct, product]);

  return (
    <ProductForm
      product={stateProduct}
      setProduct={setProduct}
      onSubmit={async (event) => {
        event.preventDefault();
        try {
          await updateProduct(stateProduct);
          history.push("/products");
        } catch (error) {
          alert(`Something went wrong: ${error.message}`);
        }
      }}
      submitButtonText="Update Product"
    />
  );
};

const mapStateToProps = ({ allergenManager, product }) => ({
  allergenManager,
  product,
});
const mapDispatchToProps = { updateProduct, getProduct, clearProduct };

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProduct);
