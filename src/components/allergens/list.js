import React from "react";
import { connect } from "react-redux";
import { Button, Icon, Image, Table } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { getAllergens } from "redux/actions/allergen-manager";
import { deleteAllergen } from "redux/actions/allergen";

const AllergenList = ({ allergenManager, deleteAllergen, getAllergens }) => {
  return (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Image</Table.HeaderCell>
          <Table.HeaderCell></Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {allergenManager.items.map((allergen) => (
          <Table.Row key={allergen.id}>
            <Table.Cell>{allergen.name}</Table.Cell>
            <Table.Cell>
              <Image size="mini" src={allergen.image} />
            </Table.Cell>
            <Table.Cell>
              <Link to={`/settings/allergens/${allergen.id}/edit`}>
                <Button>
                  <Icon name="edit" />
                  Edit
                </Button>
              </Link>
              <Button
                onClick={async (event) => {
                  await deleteAllergen(allergen.id);
                  getAllergens();
                }}
              >
                <Icon name="delete" />
                Delete
              </Button>
            </Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
};

const mapStateToProps = ({ allergenManager }) => ({
  allergenManager,
});

const mapDispatchToProps = {
  deleteAllergen,
  getAllergens,
};

export default connect(mapStateToProps, mapDispatchToProps)(AllergenList);
