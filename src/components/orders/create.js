import React, { useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

import {
  getOrder,
  clearOrder,
  approveOrder,
  declineOrder,
  createOrder,
} from "redux/actions/order";
import Order from "domain-objects/order";

import OrderForm from "components/orders/form";

const CreateOrder = ({
  order,
  orderStatusManager,
  getOrder,
  productManager,
  clearOrder,
  approveOrder,
  declineOrder,
  createOrder,
}) => {
  const history = useHistory();
  const [stateOrder, setStateOrder] = useState(new Order());

  return (
    <OrderForm
      onSubmit={(event) => {
        event.preventDefault();
        createOrder(stateOrder, (order) => history.push(`/orders/${order.id}`));
      }}
      stateOrder={stateOrder}
      setStateOrder={setStateOrder}
    />
  );
};

const mapStateToProps = ({ order, orderStatusManager, productManager }) => ({
  order,
  orderStatusManager,
  productManager,
});

const mapDispatchToProps = {
  getOrder,
  clearOrder,
  approveOrder,
  declineOrder,
  createOrder,
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateOrder);
