import React, { useState } from "react";
import { connect } from "react-redux";
import { Button, Checkbox, Table } from "semantic-ui-react";
import { Link } from "react-router-dom";

const SelectOrdersToPrint = ({ orderManager, orderStatusManager }) => {
  const [ordersToPrint, setOrdersToPrint] = useState([]);
  const addOrderToPrint = (order) => {
    if (ordersToPrint.length <= 3) {
      setOrdersToPrint([...ordersToPrint, order]);
    }
  };
  const removeOrderToPrint = (order) => {
    setOrdersToPrint([
      ...ordersToPrint.slice(0, ordersToPrint.indexOf(order)),
      ...ordersToPrint.slice(
        ordersToPrint.indexOf(order) + 1,
        ordersToPrint.length
      ),
    ]);
  };
  const toggleCheckbox = (checked, order) => {
    if (checked === true) {
      addOrderToPrint(order);
    } else {
      removeOrderToPrint(order);
    }
  };
  return (
    <div>
      Select up to 4 receipts
      <Button
        as={Link}
        to={`/orders/print-receipts/${ordersToPrint
          .map((order) => order.id)
          .join("/")}`}
        disabled={ordersToPrint.length < 1}
      >
        Print Receipts
      </Button>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell></Table.HeaderCell>
            <Table.HeaderCell>Customer name</Table.HeaderCell>
            <Table.HeaderCell>Delivery date</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderManager
            .filterStatus(orderStatusManager.getByStatus("approved").id)
            .items.map((order) => (
              <Table.Row>
                <Table.Cell>
                  <Checkbox
                    checked={ordersToPrint.indexOf(order) !== -1}
                    onChange={(event, data) =>
                      toggleCheckbox(data.checked, order)
                    }
                  />
                </Table.Cell>
                <Table.Cell>
                  {order.customer.first_name} {order.customer.last_name}
                </Table.Cell>
                <Table.Cell>
                  {order.delivery.formatField("delivery_date")}
                </Table.Cell>
              </Table.Row>
            ))}
        </Table.Body>
      </Table>
    </div>
  );
};

const mapStateToProps = ({ orderManager, orderStatusManager }) => ({
  orderManager,
  orderStatusManager,
});

export default connect(mapStateToProps)(SelectOrdersToPrint);
