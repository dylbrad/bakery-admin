import React from "react";
import { connect } from "react-redux";
import { Button, Form, Table } from "semantic-ui-react";

const EditTrolley = ({
  productManager,
  trolley,
  withAllergens = false,
  allergenManager,
  setStateTrolley,
  stateTrolley,
}) => {
  return (
    <Table unstackable>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Product</Table.HeaderCell>
          <Table.HeaderCell textAlign="right">Quantity</Table.HeaderCell>
          <Table.HeaderCell textAlign="right">Unit Price</Table.HeaderCell>
          <Table.HeaderCell textAlign="right">Price</Table.HeaderCell>
          <Table.HeaderCell></Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {stateTrolley.items.map((item) => {
          const product = productManager.getProduct(item.product_id);
          return (
            <Table.Row key={product.id}>
              <Table.Cell>{product.name}</Table.Cell>
              <Table.Cell textAlign="right">
                <input
                  placeholder="quantity"
                  name="quantity"
                  id="quantity"
                  value={item.quantity}
                  onChange={(event) =>
                    setStateTrolley(
                      stateTrolley.updateTrolleyItem(
                        item.update(event.target.name, event.target.value)
                      )
                    )
                  }
                />
              </Table.Cell>
              <Table.Cell textAlign="right">
                <Form.Field>
                  <input
                    placeholder="unit_price"
                    name="unit_price"
                    id="unit_price"
                    value={item.unit_price}
                    onChange={(event) =>
                      setStateTrolley(
                        stateTrolley.updateTrolleyItem(
                          item.update(event.target.name, event.target.value)
                        )
                      )
                    }
                  />
                </Form.Field>
              </Table.Cell>
              <Table.Cell textAlign="right">
                {Math.round(item.quantity * item.unit_price * 100) / 100}
              </Table.Cell>
              <Table.Cell>
                <Button
                  color="red"
                  onClick={(event) => {
                    setStateTrolley(stateTrolley.deleteItem(product));
                  }}
                  type="button"
                >
                  Delete
                </Button>
              </Table.Cell>
            </Table.Row>
          );
        })}
        <Table.Row>
          <Table.Cell>Delivery</Table.Cell>
          <Table.Cell></Table.Cell>
          <Table.Cell></Table.Cell>
          <Table.Cell textAlign="right">
            <Form.Field>
              <input
                placeholder="delivery_price"
                name="delivery_price"
                id="delivery_price"
                value={stateTrolley.delivery_price}
                onChange={(event) =>
                  setStateTrolley(
                    stateTrolley.update(event.target.name, event.target.value)
                  )
                }
              />
            </Form.Field>
          </Table.Cell>
          <Table.Cell></Table.Cell>
        </Table.Row>
      </Table.Body>
      <Table.Footer>
        <Table.Row>
          <Table.HeaderCell>Total</Table.HeaderCell>
          <Table.HeaderCell></Table.HeaderCell>
          <Table.HeaderCell></Table.HeaderCell>
          <Table.HeaderCell textAlign="right">
            €{stateTrolley.calculateTotal()}
          </Table.HeaderCell>
          <Table.HeaderCell></Table.HeaderCell>
        </Table.Row>
      </Table.Footer>
    </Table>
  );
};

const mapStateToProps = ({ productManager, allergenManager }) => ({
  productManager,
  allergenManager,
});

export default connect(mapStateToProps)(EditTrolley);
