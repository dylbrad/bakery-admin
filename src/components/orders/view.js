import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Button, Grid, Header, Icon, Menu, Segment } from "semantic-ui-react";

import {
  getOrder,
  clearOrder,
  approveOrder,
  declineOrder,
} from "redux/actions/order";
import Order from "domain-objects/order";

import Placeholder from "components/placeholder";
import CustomerCard from "components/customer-card";
import TrolleySmall from "components/trolleys/trolley-small";
import OrderStatusForm from "components/orders/order-status-form";
import TextAreaModal from "components/orders/textarea-modal";

const ViewOrder = ({
  order,
  orderStatusManager,
  getOrder,
  productManager,
  clearOrder,
  approveOrder,
  declineOrder,
  specialOfferManager,
}) => {
  const [stateOrder, setStateOrder] = useState(new Order());

  useEffect(() => {
    setStateOrder(order);
  }, [setStateOrder, order]);
  if (!order.id) {
    return <Placeholder columns={2} rows={15} />;
  }
  const status = orderStatusManager.getById(order.status_id);
  return (
    <div>
      <Menu stackable>
        <Menu.Item>Order #{order.id}</Menu.Item>
        <Menu.Item>
          Order placed on: {order.formatField("inserted_at")}
        </Menu.Item>
        <Menu.Item>
          <OrderStatusForm
            order={order}
            stateOrder={stateOrder}
            setStateOrder={setStateOrder}
            fluid
          />
        </Menu.Item>
        <Menu.Item position="right">
          <Link to={`/orders/${order.id}/edit`}>
            <Button secondary>Edit Order</Button>
          </Link>
        </Menu.Item>
        {status.status === "approved" && (
          <Menu.Item position="right">
            <Link to={`/orders/${order.id}/receipt`}>
              <Button>View Receipt</Button>
            </Link>
          </Menu.Item>
        )}

        {status.status === "confirmed" && (
          <Menu.Item position="right">
            <TextAreaModal
              title="Approve order"
              content="Approve text"
              trigger={<Button color="green">Approve</Button>}
              action={(message) =>
                approveOrder(
                  stateOrder.update("message", message),
                  setStateOrder
                )
              }
              actionButtonText="Approve"
            />
            <TextAreaModal
              actionButtonColor="red"
              actionButtonText="Decline"
              title="Decline order"
              content="Reason to decline order"
              trigger={<Button color="red">Decline</Button>}
              action={(message) =>
                declineOrder(
                  stateOrder.update("message", message),
                  setStateOrder
                )
              }
            />
          </Menu.Item>
        )}
      </Menu>
      <Grid columns={2} stackable>
        <Grid.Column>
          <Segment>
            <Header as="h3">Trolley</Header>
            <p>
              Delivery: {order.delivery.formatField("delivery_date")} between{" "}
              {order.delivery.time_slot}:00 and {order.delivery.time_slot + 1}
              :00
            </p>
            <TrolleySmall
              order={order}
              productManager={productManager}
              specialOfferManager={specialOfferManager}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment>
            <Header as="h3">Customer</Header>

            {order.is_delivery ? (
              <>
                <Icon size="big" name="bicycle" />
                Delivery
              </>
            ) : (
              <>
                <Icon size="big" name="shopping bag" />
                Collection
              </>
            )}
            <CustomerCard
              customer={order.customer}
              isDelivery={order.is_delivery}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment>
            {order.delivery.message ? (
              <>
                <Header as="h3">Customer's Message</Header>
                <cite>{order.delivery.message}</cite>
              </>
            ) : (
              <Header as="h3">No Message</Header>
            )}
          </Segment>
          <Segment>
            <Header as="h3">Baker's Message</Header>
            <cite>{order.message}</cite>
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment>
            <Header as="h3">Payment</Header>
            <p>Payment type: {order.payment.type}</p>
            <p>Transaction code: {order.payment.transaction_code}</p>
          </Segment>
        </Grid.Column>
      </Grid>
    </div>
  );
};

const mapStateToProps = ({
  order,
  orderStatusManager,
  productManager,
  specialOfferManager,
}) => ({
  order,
  orderStatusManager,
  productManager,
  specialOfferManager,
});

const mapDispatchToProps = {
  getOrder,
  clearOrder,
  approveOrder,
  declineOrder,
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewOrder);
