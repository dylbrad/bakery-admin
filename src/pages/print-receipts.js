import React, { useEffect } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import { Grid } from "semantic-ui-react";

import { ViewOrderReceipt } from "components/orders/view-receipt";
import { getProducts, clearProducts } from "redux/actions/product-manager";
import {
  getOrderStatuses,
  clearOrderStatuses,
} from "redux/actions/order-status-manager";
import { getOrders, clearOrders } from "redux/actions/order-manager";
import { getAllergens, clearAllergens } from "redux/actions/allergen-manager";
import {
  getSpecialOffers,
  clearSpecialOffers,
} from "redux/actions/special-offer-manager";
import { getSettings, clearSettings } from "redux/actions/setting-manager";

const ViewOrderReceiptPage = ({
  getProducts,
  clearProducts,
  getOrderStatuses,
  clearOrderStatuses,
  clearOrders,
  allergenManager,
  getAllergens,
  clearAllergens,
  getOrders,
  orderManager,
  productManager,
  getSpecialOffers,
  clearSpecialOffers,
  specialOfferManager,
  getSettings,
  clearSettings,
  settingManager,
}) => {
  const {
    orderId1Part1,
    orderId1Part2,
    orderId1Part3,
    orderId1Part4,
    orderId1Part5,
    orderId2Part1,
    orderId2Part2,
    orderId2Part3,
    orderId2Part4,
    orderId2Part5,
    orderId3Part1,
    orderId3Part2,
    orderId3Part3,
    orderId3Part4,
    orderId3Part5,
    orderId4Part1,
    orderId4Part2,
    orderId4Part3,
    orderId4Part4,
    orderId4Part5,
  } = useParams();
  const orderId1 = `${orderId1Part1}-${orderId1Part2}-${orderId1Part3}-${orderId1Part4}-${orderId1Part5}`;
  const orderId2 = `${orderId2Part1}-${orderId2Part2}-${orderId2Part3}-${orderId2Part4}-${orderId2Part5}`;
  const orderId3 = `${orderId3Part1}-${orderId3Part2}-${orderId3Part3}-${orderId3Part4}-${orderId3Part5}`;
  const orderId4 = `${orderId4Part1}-${orderId4Part2}-${orderId4Part3}-${orderId4Part4}-${orderId4Part5}`;

  useEffect(() => {
    async function fetch() {
      await getAllergens();
      await getProducts();
      await getSpecialOffers();
      await getOrderStatuses();
      await getOrders();
      await getSettings();
    }
    fetch();

    return () => {
      clearProducts();
      clearOrderStatuses();
      clearAllergens();
      clearOrders();
      clearSpecialOffers();
      clearSettings();
    };
  }, [
    getProducts,
    getOrderStatuses,
    getOrders,
    getAllergens,
    clearProducts,
    clearOrderStatuses,
    clearOrders,
    clearAllergens,
    getSpecialOffers,
    clearSpecialOffers,
    getSettings,
    clearSettings,
  ]);

  return (
    <Grid columns={4} style={{ marginLeft: 50, marginRight: 50 }}>
      {orderManager
        .filterByID([orderId1, orderId2, orderId3, orderId4])
        .items.map((order) => {
          return (
            <Grid.Column>
              <ViewOrderReceipt
                order={order}
                key={order.id}
                productManager={productManager}
                allergenManager={allergenManager}
                specialOfferManager={specialOfferManager}
                settingManager={settingManager}
              />
            </Grid.Column>
          );
        })}
    </Grid>
  );
};

const mapStateToProps = ({
  orderManager,
  productManager,
  allergenManager,
  specialOfferManager,
  settingManager,
}) => ({
  orderManager,
  productManager,
  allergenManager,
  specialOfferManager,
  settingManager,
});

const mapDispatchToProps = {
  getProducts,
  clearProducts,
  getOrderStatuses,
  clearOrderStatuses,
  getOrders,
  clearOrders,
  getAllergens,
  clearAllergens,
  getSpecialOffers,
  clearSpecialOffers,
  getSettings,
  clearSettings,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewOrderReceiptPage);
