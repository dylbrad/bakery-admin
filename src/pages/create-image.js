import React from "react";
import { Segment, Header } from "semantic-ui-react";

import CreateImage from "components/images/create";

const CreateImagePage = () => (
  <Segment>
    <Header as="h1">Create Image</Header>
    <CreateImage />
  </Segment>
);

export default CreateImagePage;
