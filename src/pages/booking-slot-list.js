import React from "react";
import { Segment, Header } from "semantic-ui-react";

import BookingSlotList from "components/booking-slots/list";

const BookingSlotListPage = () => (
  <Segment>
    <Header as="h1">Booking Slots</Header>
    <BookingSlotList />
  </Segment>
);

export default BookingSlotListPage;
