import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Button, Segment, Header, Menu } from "semantic-ui-react";
import { Link, useLocation } from "react-router-dom";

import OrderSummary from "components/orders/summary";
import OrderList from "components/orders/list";
import { getOrders, clearOrders } from "redux/actions/order-manager";
import { getProducts, clearProducts } from "redux/actions/product-manager";
import {
  getOrderStatuses,
  clearOrderStatuses,
} from "redux/actions/order-status-manager";
import StatusTabBar from "components/orders/status-tab-bar";
import {
  clearSpecialOffers,
  getSpecialOffers,
} from "redux/actions/special-offer-manager";

const ApprovedOrderListPage = ({
  getOrders,
  getProducts,
  clearProducts,
  clearOrders,
  orderManager,
  status,
  orderStatusManager,
  getOrderStatuses,
  clearOrderStatuses,
  clearSpecialOffers,
  getSpecialOffers,
  specialOfferManager,
}) => {
  const { pathname } = useLocation();
  const dateFilter =
    (pathname.endsWith("today") && "today") ||
    (pathname.endsWith("tomorrow") ? "tomorrow" : null);
  useEffect(() => {
    async function fetch() {
      await getOrderStatuses();
      await getProducts();
      await getSpecialOffers();
      await getOrders();
    }
    fetch();
    return () => {
      clearProducts();
      clearOrders();
      clearOrderStatuses();
      clearSpecialOffers();
    };
  }, [
    getProducts,
    getOrders,
    clearProducts,
    clearOrders,
    getOrderStatuses,
    clearOrderStatuses,
    getSpecialOffers,
    clearSpecialOffers,
  ]);
  return (
    <Segment>
      <Header as="h1">Orders</Header>
      <StatusTabBar status={status} />
      <Menu>
        <Menu.Item
          active={pathname === "/orders/approved/today"}
          as={Link}
          to="/orders/approved/today"
        >
          Today's orders
        </Menu.Item>
        <Menu.Item
          active={pathname === "/orders/approved/tomorrow"}
          as={Link}
          to="/orders/approved/tomorrow"
        >
          Tomorrow's orders
        </Menu.Item>
        <Menu.Item
          active={pathname === "/orders/approved"}
          as={Link}
          to="/orders/approved"
        >
          All
        </Menu.Item>
      </Menu>
      <OrderSummary
        summary={orderManager
          .filterStatus(orderStatusManager.getByStatus(status).id)
          .filterDate(dateFilter)

          .groupByProduct(specialOfferManager)}
      />
      <Link to="/orders/print-receipts">
        <Button>Print receipts</Button>
      </Link>
      <OrderList status={status} dateFilter={dateFilter} />
    </Segment>
  );
};

const mapStateToProps = ({
  orderManager,
  orderStatusManager,
  specialOfferManager,
}) => ({
  orderManager,
  orderStatusManager,
  specialOfferManager,
});

const mapDispatchToProps = {
  getOrders,
  getProducts,
  clearProducts,
  clearOrders,
  getOrderStatuses,
  clearOrderStatuses,
  getSpecialOffers,
  clearSpecialOffers,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApprovedOrderListPage);
