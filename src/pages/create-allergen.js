import React from "react";
import { Segment, Header } from "semantic-ui-react";

import CreateAllergen from "components/allergens/create";

const CreateAllergenPage = () => (
  <Segment>
    <Header as="h1">Create Allergen</Header>
    <CreateAllergen />
  </Segment>
);

export default CreateAllergenPage;
