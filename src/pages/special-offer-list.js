import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Button, Segment, Header } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { getSpecialOffers } from "redux/actions/special-offer-manager";
import { getProducts } from "redux/actions/product-manager";
import SpecialOfferList from "components/special-offers/list";

const SpecialOfferListPage = ({
  getSpecialOffers,
  specialOfferManager,
  getProducts,
}) => {
  useEffect(() => {
    getSpecialOffers();
    getProducts();
  }, [getSpecialOffers, getProducts]);

  return (
    <Segment>
      <Header as="h1">Special Offers</Header>
      <Link to="/special-offers/new">
        <Button>New special offer</Button>
      </Link>
      <SpecialOfferList />
    </Segment>
  );
};

const mapStateToProps = ({ specialOfferManager }) => ({
  specialOfferManager,
});

const mapDispatchToProps = {
  getSpecialOffers,
  getProducts,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SpecialOfferListPage);
