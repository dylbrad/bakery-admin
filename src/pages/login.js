import React from "react";
import { Segment } from "semantic-ui-react";

import Login from "components/login";

const LoginPage = () => (
  <Segment>
    <Login />
  </Segment>
);

export default LoginPage;
