import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";
import { useParams } from "react-router-dom";

import { getRecipe, clearRecipe } from "redux/actions/recipe";
import { getProducts, clearProducts } from "redux/actions/product-manager";
import UpdateRecipe from "components/recipes/update";

const UpdateRecipePage = ({
  getRecipe,
  getProducts,
  clearProducts,
  clearRecipe,
  productManager,
  recipe,
}) => {
  const { recipeId } = useParams();
  useEffect(() => {
    getProducts();
    getRecipe(recipeId);
    return () => {
      clearProducts();
      clearRecipe();
    };
  }, [getProducts, getRecipe, clearProducts, clearRecipe, recipeId]);
  return (
    <Segment>
      <Header as="h1">Update Recipe</Header>
      <UpdateRecipe />
    </Segment>
  );
};

const mapStateToProps = ({ orderManager }) => ({
  orderManager,
});

const mapDispatchToProps = {
  getRecipe,
  getProducts,
  clearProducts,
  clearRecipe,
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateRecipePage);
