import React, { useEffect } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";

import ViewOrderReceipt from "components/orders/view-receipt";
import { getProducts, clearProducts } from "redux/actions/product-manager";
import {
  getOrderStatuses,
  clearOrderStatuses,
} from "redux/actions/order-status-manager";
import { getOrder, clearOrder } from "redux/actions/order";
import { getAllergens, clearAllergens } from "redux/actions/allergen-manager";
import {
  getSpecialOffers,
  clearSpecialOffers,
} from "redux/actions/special-offer-manager";
import { getSettings, clearSettings } from "redux/actions/setting-manager";

const ViewOrderReceiptPage = ({
  getProducts,
  clearProducts,
  getOrderStatuses,
  clearOrderStatuses,
  clearOrder,
  allergenManager,
  getAllergens,
  clearAllergens,
  clearSpecialOffers,
  getOrder,
  getSpecialOffers,
  settingManager,
  getSettings,
  clearSettings,
}) => {
  const { id } = useParams();

  useEffect(() => {
    async function fetch() {
      await getAllergens();
      await getProducts();
      await getOrderStatuses();
      await getSpecialOffers();
      await getOrder(id);
      await getSettings();
    }
    fetch();

    return () => {
      clearProducts();
      clearOrderStatuses();
      clearAllergens();
      clearOrder();
      clearSpecialOffers();
      clearSettings();
    };
  }, [
    getProducts,
    getOrderStatuses,
    getOrder,
    getAllergens,
    getSpecialOffers,
    id,
    clearProducts,
    clearOrderStatuses,
    clearOrder,
    clearAllergens,
    clearSpecialOffers,
    getSettings,
    clearSettings,
  ]);

  return <ViewOrderReceipt />;
};

const mapStateToProps = ({ settingManager }) => ({ settingManager });

const mapDispatchToProps = {
  getProducts,
  clearProducts,
  getOrderStatuses,
  clearOrderStatuses,
  getOrder,
  clearOrder,
  getAllergens,
  clearAllergens,
  clearSpecialOffers,
  getSpecialOffers,
  getSettings,
  clearSettings,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewOrderReceiptPage);
