import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { getImages, clearImages } from "redux/actions/image-manager";
import ImageList from "components/images/list";

const ImageListPage = ({ getImages, imageManager, clearImages }) => {
  useEffect(() => {
    getImages();
    return () => clearImages();
  }, [getImages, clearImages]);
  return (
    <Segment>
      <Header as="h1">Images</Header>
      <Link to="/images/new">New image</Link>
      <ImageList />
    </Segment>
  );
};

const mapStateToProps = ({ imageManager }) => ({
  imageManager,
});

const mapDispatchToProps = {
  getImages,
  clearImages,
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageListPage);
