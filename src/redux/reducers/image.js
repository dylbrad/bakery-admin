import { UPLOAD_IMAGE, CLEAR_IMAGE } from "redux/actions/action-types";

import Image from "domain-objects/image";

export function imageReducer(state = new Image(), action) {
  switch (action.type) {
    case UPLOAD_IMAGE: {
      return action.payload;
    }
    case CLEAR_IMAGE: {
      return new Image();
    }
    default: {
      return state;
    }
  }
}
