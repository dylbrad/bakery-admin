import { GET_IMAGES, CLEAR_IMAGES } from "redux/actions/action-types";

import ImageManager from "domain-objects/image-manager";

export function imageManagerReducer(state = new ImageManager(), action) {
  switch (action.type) {
    case GET_IMAGES: {
      return action.payload;
    }
    case CLEAR_IMAGES: {
      return new ImageManager();
    }
    default: {
      return state;
    }
  }
}
