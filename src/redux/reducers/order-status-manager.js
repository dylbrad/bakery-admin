import {
  GET_ORDER_STATUSES,
  CLEAR_ORDER_STATUSES,
} from "redux/actions/action-types";

import OrderStatusManager from "domain-objects/order-status-manager";

export function orderStatusManagerReducer(
  state = new OrderStatusManager(),
  action
) {
  switch (action.type) {
    case GET_ORDER_STATUSES: {
      return action.payload;
    }
    case CLEAR_ORDER_STATUSES: {
      return new OrderStatusManager();
    }
    default: {
      return state;
    }
  }
}
