import axios from "redux/actions/axios";
import { UPLOAD_IMAGE, CLEAR_IMAGE } from "redux/actions/action-types";

import Image from "domain-objects/image";

export function uploadImage(image) {
  const data = new FormData();
  data.append("image", image);
  return (dispatch, getState) => {
    axios
      .post("/admin/images", data)
      .then((response) =>
        dispatch({ type: UPLOAD_IMAGE, payload: new Image(response.data.data) })
      );
  };
}

export function deleteImage(id) {
  return (dispatch, getState) => {
    axios
      .delete(`/admin/images/${id}`)
      .then((response) => clearImage()(dispatch, getState));
  };
}

export function clearImage() {
  return (dispatch, getState) => dispatch({ type: CLEAR_IMAGE });
}
