import axios from "redux/actions/axios";
import {
  GET_ALLERGEN,
  CREATE_ALLERGEN,
  UPDATE_ALLERGEN,
  CLEAR_ALLERGEN,
} from "redux/actions/action-types";

import Allergen from "domain-objects/allergen";

export function getAllergen(id) {
  return (dispatch, getState) => {
    axios.get(`/admin/allergens/${id}`).then((response) =>
      dispatch({
        type: GET_ALLERGEN,
        payload: new Allergen(response.data.data),
      })
    );
  };
}

export function createAllergen(allergen) {
  return (dispatch, getState) => {
    axios
      .post("/admin/allergens", { allergen: allergen.renderJSON() })
      .then((response) =>
        dispatch({
          type: CREATE_ALLERGEN,
          payload: new Allergen(response.data.data),
        })
      )
      .catch((error) => console.log(error));
  };
}

export function updateAllergen(allergen) {
  return (dispatch, getState) => {
    axios
      .put(`/admin/allergens/${allergen.id}/`, {
        allergen: allergen.renderJSON(),
      })
      .then((response) =>
        dispatch({
          type: UPDATE_ALLERGEN,
          payload: new Allergen(response.data.data),
        })
      )
      .catch((error) => console.log(error));
  };
}

export function deleteAllergen(id) {
  return (dispatch, getState) => {
    axios
      .delete(`/admin/allergens/${id}`)
      .then((response) => clearAllergen()(dispatch, getState));
  };
}

export function clearAllergen() {
  return (dispatch, getState) => dispatch({ type: CLEAR_ALLERGEN });
}
