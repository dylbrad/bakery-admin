import axios from "redux/actions/axios";
import { GET_MESSAGES, CLEAR_MESSAGES } from "redux/actions/action-types";

import { createMessageManager } from "domain-objects/message-manager";

export function getMessages() {
  return (dispatch, getState) => {
    axios.get("/admin/messages/").then((response) =>
      dispatch({
        type: GET_MESSAGES,
        payload: createMessageManager(response.data.data),
      })
    );
  };
}

export function clearMessages() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_MESSAGES });
  };
}
