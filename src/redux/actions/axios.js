import axios from "axios";
import Cookies from "js-cookie";

const axiosConfig = {
  baseURL: process.env.REACT_APP_BACKEND_HOST || "http://localhost:4000/api",
  headers: {
    "Content-Type": "application/json",
  },
};

const axiosInstance = axios.create(axiosConfig);
const jwt = Cookies.get("jwt");

if (jwt) {
  axiosInstance.defaults.headers.common.Authorization = `Bearer ${jwt}`;
}

axiosInstance.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response && error.response.status === 401) {
      Cookies.remove("jwt");
      window.location.replace("/login");
    }
    return Promise.reject(error);
  }
);

export default axiosInstance;
