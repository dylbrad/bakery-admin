import axios from "redux/actions/axios";
import {
  GET_SPECIAL_OFFERS,
  CLEAR_SPECIAL_OFFERS,
} from "redux/actions/action-types";

import { createSpecialOfferManager } from "domain-objects/special-offer-manager";

export function getSpecialOffers() {
  return (dispatch, getState) => {
    axios.get("/admin/special_offers/").then((response) =>
      dispatch({
        type: GET_SPECIAL_OFFERS,
        payload: createSpecialOfferManager(response.data.data),
      })
    );
  };
}

export function clearSpecialOffers() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_SPECIAL_OFFERS });
  };
}
