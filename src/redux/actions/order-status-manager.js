import axios from "redux/actions/axios";
import {
  GET_ORDER_STATUSES,
  CLEAR_ORDER_STATUSES,
} from "redux/actions/action-types";

import OrderStatusManager from "domain-objects/order-status-manager";
import OrderStatus from "domain-objects/order-status";

export function getOrderStatuses() {
  return (dispatch, getState) => {
    axios.get("/order_statuses/").then((response) =>
      dispatch({
        type: GET_ORDER_STATUSES,
        payload: new OrderStatusManager(
          response.data.data.map((orderStatus) => new OrderStatus(orderStatus))
        ),
      })
    );
  };
}

export function clearOrderStatuses() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_ORDER_STATUSES });
  };
}
