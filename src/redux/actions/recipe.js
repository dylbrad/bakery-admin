import axios from "redux/actions/axios";
import {
  UPDATE_RECIPE,
  CLEAR_RECIPE,
  GET_RECIPE,
} from "redux/actions/action-types";

import Recipe from "domain-objects/recipe";

export function getRecipe(recipeId) {
  return (dispatch, getState) => {
    axios.get(`/admin/recipes/${recipeId}/`).then((response) => {
      dispatch({
        type: GET_RECIPE,
        payload: new Recipe(response.data.data),
      });
    });
  };
}

export function updateRecipe(recipe) {
  return async (dispatch, getState) => {
    await axios
      .put(`/admin/recipes/${recipe.id}/`, recipe.renderJSON())
      .then((response) => {
        dispatch({ type: UPDATE_RECIPE, payload: new Recipe(recipe) });
      });
  };
}

export function clearRecipe() {
  return (dispatch, getState) => dispatch({ type: CLEAR_RECIPE });
}
