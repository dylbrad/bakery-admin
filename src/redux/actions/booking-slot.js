import axios from "redux/actions/axios";
import {
  ADD_BOOKING_SLOT,
  REMOVE_BOOKING_SLOT,
} from "redux/actions/action-types";

import { range } from "utils/util";
import BookingSlot from "domain-objects/booking-slot";

export function addBookingSlots(bookingSlot, quantity) {
  return (dispatch, getState) =>
    range(quantity).map(() => addBookingSlot(bookingSlot)(dispatch, getState));
}

export function addBookingSlot(bookingSlot) {
  return (dispatch, getState) => {
    axios
      .post("/admin/booking_slots", { booking_slot: bookingSlot.renderJSON() })
      .then((response) =>
        dispatch({
          type: ADD_BOOKING_SLOT,
          payload: new BookingSlot(response.data.data),
        })
      );
  };
}

export function resetBookingSlots(bookingSlotManager) {
  return (dispatch, getState) => {
    bookingSlotManager.items.forEach((bookingSlot) => {
      const id = bookingSlot.id;
      axios
        .delete(`/admin/booking_slots/${id}`)
        .then((response) =>
          dispatch({ type: REMOVE_BOOKING_SLOT, payload: id })
        );
    });
  };
}
