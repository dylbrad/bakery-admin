import axios from "redux/actions/axios";
import {
  CREATE_SPECIAL_OFFER,
  UPDATE_SPECIAL_OFFER,
  GET_SPECIAL_OFFER,
  CLEAR_SPECIAL_OFFER,
} from "redux/actions/action-types";

import SpecialOffer from "domain-objects/special-offer";

export function getSpecialOffer(specialOfferID) {
  return (dispatch, getState) => {
    axios.get(`/admin/special_offers/${specialOfferID}`).then((response) =>
      dispatch({
        type: GET_SPECIAL_OFFER,
        payload: new SpecialOffer(response.data.data),
      })
    );
  };
}

export function createSpecialOffer(specialOffer) {
  return (dispatch, getState) => {
    axios
      .post("/admin/special_offers", {
        special_offer: specialOffer.renderJSON(),
      })
      .then((response) =>
        dispatch({
          type: CREATE_SPECIAL_OFFER,
          payload: new SpecialOffer(response.data.data),
        })
      )
      .catch((error) => console.log(error));
  };
}

export function updateSpecialOffer(specialOffer) {
  return (dispatch, getState) => {
    axios
      .put(`/admin/special_offers/${specialOffer.id}`, {
        special_offer: specialOffer.renderJSON(),
      })
      .then((response) =>
        dispatch({
          type: UPDATE_SPECIAL_OFFER,
          payload: new SpecialOffer(response.data.data),
        })
      )
      .catch((error) => console.log(error));
  };
}

export function clearSpecialOffer() {
  return (dispatch, getState) => dispatch({ type: CLEAR_SPECIAL_OFFER });
}
