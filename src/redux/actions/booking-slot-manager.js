import axios from "redux/actions/axios";
import {
  GET_BOOKING_SLOTS,
  CLEAR_BOOKING_SLOTS,
} from "redux/actions/action-types";

import { createBookingSlotManager } from "domain-objects/booking-slot-manager";
import { formatDateToBackend } from "utils/date";

export function getBookingSlots(date = formatDateToBackend(new Date())) {
  return (dispatch, getState) => {
    axios.get(`/admin/booking_slots/?date=${date}`).then((response) =>
      dispatch({
        type: GET_BOOKING_SLOTS,
        payload: createBookingSlotManager(response.data.data),
      })
    );
  };
}

export function clearBookingSlots() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_BOOKING_SLOTS });
  };
}
