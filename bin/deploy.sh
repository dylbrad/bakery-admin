#!/bin/bash

read -p "Are you sure you want to deploy PRODUCTION? (y/n) " -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    now=$(date +'%Y-%m-%d-%H-%M')
    git_branch=$(git rev-parse --abbrev-ref HEAD)
    filename=$now-$git_branch
    ssh_host=bakery

    scp -r build $ssh_host:bakery-admin/$filename
    ssh bakery "rm bakery-admin/latest; ln -s /home/$ssh_host/bakery-admin/$filename /home/$ssh_host/bakery-admin/latest"
fi
